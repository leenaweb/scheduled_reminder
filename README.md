# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 2.4.1

* Rails version 5.2.3

* Update database.yml update PGHOST and POSTGRES_USER

* Create database 'rake db:create'

* Migrate 'rake db:migrate'

* Run server 'rails server'

* I have used delayed_job_active_record gem for background jobs

* User can create reminder and set date on which he wants  to send the reminder
  user can see all the reminders.

* User can delete the reminder.

* I have established association between user and reminders ,user can have many reminders
