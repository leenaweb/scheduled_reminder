class ReminderMailer < ApplicationMailer
	default from: "from@example.com"

  def reminder_mail(user, reminder)
    @user = user
    @reminder = reminder
    mail(to: @user.email, subject: 'Reminder Mail')
  end

end
