class Reminder < ApplicationRecord
	belongs_to :user
	after_save :reminder
	validates_presence_of :title, :description, :reminder_date

  def reminder
  	ReminderMailer.reminder_mail(self.user, self).deliver		
  end

  def when_to_run
  	reminder_date + 6.hour + 30.minutes
  end

  handle_asynchronously :reminder, :run_at => Proc.new { |i|  i.when_to_run}
end
