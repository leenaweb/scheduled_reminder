Rails.application.routes.draw do
  resources :reminder_dates, except: %i[index]
  resources :reminders
  devise_for :users
  root 'reminders#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
