class AddReminderDateToReminder < ActiveRecord::Migration[5.2]
  def change
    add_column :reminders, :reminder_date, :datetime
  end
end
