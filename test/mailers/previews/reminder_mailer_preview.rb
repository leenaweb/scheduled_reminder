# Preview all emails at http://localhost:3000/rails/mailers/reminder_mailer
class ReminderMailerPreview < ActionMailer::Preview
  def sample_mail_preview
    ReminderMailer.reminder_mail(User.first, Reminder.first)
  end

end
