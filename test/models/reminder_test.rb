require 'test_helper'

class ReminderTest < ActiveSupport::TestCase

	test "should not save reminder without title" do
	  reminder = Reminder.new
	  assert_not reminder.save
	end

	test "should save reminder and check job count" do
	  reminder = Reminder.new(title: 'test title', description: 'test description', reminder_date: DateTime.now + 1.months)
	  assert reminder.save
	  assert_equal(1, Delayed::Job.count)	
	end
end
